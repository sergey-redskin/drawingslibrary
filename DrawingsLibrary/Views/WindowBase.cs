﻿using System;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Media;

using MahApps.Metro.Controls;


namespace DrawingsLibrary.Views
{
    public abstract class WindowBase : MetroWindow
    {
        protected WindowBase()
        {
            ShowIconOnTitleBar = false;
            TitleCharacterCasing = CharacterCasing.Normal;
            UseLayoutRounding = true;

            try
            {
                GlowBrush = FindResource("AccentColorBrush") as Brush;
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception);
            }
        }

        protected static bool IsInDesignMode
        {
            get
            {
                Process process = Process.GetCurrentProcess();
                bool result = process.ProcessName == "devenv";
                process.Dispose();
                return result;
            }
        }
    }
}