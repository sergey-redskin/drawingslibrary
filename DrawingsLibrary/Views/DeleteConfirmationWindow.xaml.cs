﻿using System.Windows;

namespace DrawingsLibrary.Views
{
    public partial class DeleteConfirmationWindow
    {
        public DeleteConfirmationWindow()
        {
            InitializeComponent();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
