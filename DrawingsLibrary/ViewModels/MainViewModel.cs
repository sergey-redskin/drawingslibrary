﻿using DrawingsLibrary.Database;
using DrawingsLibrary.Properties;
using DrawingsLibrary.Service;
using DrawingsLibrary.Views;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace DrawingsLibrary.ViewModels
{
    internal class MainViewModel : ViewModelBase
    {
        public MainViewModel()
        {
            Files.CollectionChanged += Files_CollectionChanged;
        }

        public ObservableCollection<DrawingFile> Files
        {
            get;
        } = new ObservableCollection<DrawingFile>();

        private readonly Dictionary<string, DrawingFile> HashFileDict = new Dictionary<string, DrawingFile>(StringComparer.OrdinalIgnoreCase);

        public bool DoNotRequestDeleteConfirmation
        {
            get => Settings.Default.DoNotRequestDeleteConfirmation;
            set
            {
                Settings.Default.DoNotRequestDeleteConfirmation = value;
                Settings.Default.Save();
                RaisePropertyChanged();
            }
        }

        private DrawingFile _selectedFile;

        public DrawingFile SelectedFile
        {
            get => _selectedFile;
            set
            {
                if (ReferenceEquals(_selectedFile, value))
                {
                    return;
                }

                _selectedFile = value;
                RaisePropertyChanged();
            }
        }

        private void Files_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e is null)
            {
                return;
            }

            IList newItems = e.NewItems;
            if (newItems != null)
            {
                foreach (object item in newItems)
                {
                    if (item is DrawingFile file)
                    {
                        string hash = file.Hash;
                        if (!HashFileDict.ContainsKey(hash))
                        {
                            HashFileDict.Add(hash, file);
                        }
                    }
                }
            }

            IList oldItems = e.OldItems;
            if (oldItems != null)
            {
                foreach (object item in oldItems)
                {
                    if (item is DrawingFile file)
                    {
                        string hash = file.Hash;
                        if (HashFileDict.ContainsKey(hash))
                        {
                            HashFileDict.Remove(hash);
                        }
                    }
                }
            }
        }

        private ICommand _addNewFileCommand;

        public ICommand AddNewFileCommand => _addNewFileCommand ?? (_addNewFileCommand = new RelayCommand<Window>(AddNewFile));

        private ICommand _editSelectedFileCommand;

        public ICommand EditSelectedFileCommand =>
            _editSelectedFileCommand ?? (_editSelectedFileCommand = new RelayCommand<Window>(window => { EditFile(SelectedFile, window); }, window => { return SelectedFile != null; }));

        private ICommand _getFilesCommand;

        public ICommand GetFilesCommand => _getFilesCommand ?? (_getFilesCommand = new RelayCommand(UpdateFilesCollection));

        private ICommand _deleteSelectedFileCommand;

        public ICommand DeleteSelectedFileCommand =>
            _deleteSelectedFileCommand ?? (_deleteSelectedFileCommand = new RelayCommand<Window>(window => { DeleteFile(SelectedFile, window); }, window => { return SelectedFile != null; }));

        private ICommand _showConnectionCommand;

        public ICommand ShowConnectionCommand => _showConnectionCommand ?? (_showConnectionCommand = new RelayCommand<Window>(ShowConnectionWindow));

        private void AddNewFile(Window window)
        {
            SelectedFile = null;

            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult openResult = dialog.ShowDialog();
            if (openResult == DialogResult.OK)
            {
                string filePath = dialog.FileName;
                if (File.Exists(filePath))
                {
                    string hash = Singleton.GetHash(filePath);
                    if (!string.IsNullOrWhiteSpace(hash))
                    {
                        string extension;
                        try
                        {
                            extension = Path.GetExtension(filePath)?.ToLower();
                        }
                        catch (Exception)
                        {
                            extension = null;
                        }

                        if (!string.IsNullOrWhiteSpace(extension))
                        {
                            string name;
                            try
                            {
                                name = Path.GetFileNameWithoutExtension(filePath);
                            }
                            catch (Exception)
                            {
                                name = null;
                            }

                            DrawingFile file = new DrawingFile(hash, extension, filePath)
                            {
                                Name = name
                            };

                            DrawingFileWindow drawingWindow = new DrawingFileWindow
                            {
                                DataContext = file,
                                Owner = window
                            };

                            bool? saveResult = drawingWindow.ShowDialog();
                            if (saveResult is true)
                            {
                                SelectedFile = AddOrUpdateFile(file);
                            }
                        }
                    }
                }
            }
        }

        private DrawingFile AddOrUpdateFile(DrawingFile file)
        {
            if (Files.Contains(file))
            {
                return file;
            }
            else if (HashFileDict.ContainsKey(file.Hash))
            {
                DrawingFile containedFile = HashFileDict[file.Hash];
                containedFile.Extension = file.Extension;
                containedFile.Name = file.Name;
                containedFile.Description = file.Description;
                return containedFile;
            }
            else
            {
                Files.Add(file);
                return file;
            }
        }

        private void DeleteFile(DrawingFile file, Window window)
        {
            bool toContinue;
            if (!DoNotRequestDeleteConfirmation)
            {
                DeleteConfirmationWindow confirmationWindow = new DeleteConfirmationWindow
                {
                    Owner = window
                };
                bool? confirmationResult = confirmationWindow.ShowDialog();
                toContinue = confirmationResult is true;
            }
            else
            {
                toContinue = true;
            }

            if (!toContinue)
            {
                return;
            }

            if (file is null)
            {
                return;
            }

            string filePath = Singleton.CompileFilePath(file.Hash, file.Extension);
            if (File.Exists(filePath))
            {
                try
                {
                    File.Delete(filePath);
                }
                catch (Exception)
                {
                    return;
                }

                string libraryPath = Singleton.Instance.LibraryPath;
                string directoryPath = Path.GetDirectoryName(filePath);

                while (!string.Equals(directoryPath, libraryPath, StringComparison.OrdinalIgnoreCase))
                {
                    try
                    {
                        int directoryCount = Directory.GetDirectories(directoryPath).Length;
                        if (directoryCount <= 0)
                        {
                            int fileCount = Directory.GetFiles(directoryPath).Length;
                            if (fileCount <= 0)
                            {
                                Directory.Delete(directoryPath);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        break;
                    }

                    directoryPath = Path.GetDirectoryName(directoryPath);
                };
            }

            DatabaseWriter.CallProcedure_DeleteDrawingFile(file);

            if (Files.Contains(file))
            {
                Files.Remove(file);
            }
        }

        private void EditFile(DrawingFile file, Window window)
        {
            if (file is null)
            {
                return;
            }

            DrawingFile fileClone = new DrawingFile(file.Hash, file.Extension, file.Name, file.Description);

            DrawingFileWindow drawingWindow = new DrawingFileWindow
            {
                DataContext = fileClone,
                Owner = window
            };

            bool? saveResilt = drawingWindow.ShowDialog();
            if (saveResilt is true)
            {
                SelectedFile = AddOrUpdateFile(fileClone);
            }
        }

        private static List<DrawingFile> LoadFilesFromDatabase()
        {
            List<DrawingFile> files = new List<DrawingFile>();

            SqlConnection connection = Singleton.Instance.Connection;
            if (connection is null)
            {
                return files;
            }

            const string query = "SELECT Hash, Extension, Name, Description FROM dbo.DrawingFiles;";
            SqlCommand command = new SqlCommand(query, connection);

            try
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string hash = reader["Hash"] as string;
                        string extension = reader["Extension"] as string;
                        string name = reader["Name"] as string;
                        string description = reader["Description"] as string;

                        DrawingFile file = new DrawingFile(hash, extension, name, description);
                        files.Add(file);
                    }
                }
                reader.Close();
            }
            catch (Exception)
            {
                //
            }
            finally
            {
                connection.Close();
            }

            return files;
        }

        private void ShowConnectionWindow(Window window)
        {
            ConnectionWindow connectionWindow = new ConnectionWindow
            {
                Owner = window
            };
            connectionWindow.ShowDialog();

            if (GetFilesCommand.CanExecute(null))
            {
                GetFilesCommand.Execute(null);
            }
        }

        private void UpdateFilesCollection()
        {
            Files.Clear();
            foreach (DrawingFile file in LoadFilesFromDatabase())
            {
                Files.Add(file);
            }
        }
    }
}
