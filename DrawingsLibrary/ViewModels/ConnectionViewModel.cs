﻿using DrawingsLibrary.Properties;
using DrawingsLibrary.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Security;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;

namespace DrawingsLibrary.ViewModels
{
    internal class ConnectionViewModel : ViewModelBase
    {
        public ConnectionViewModel()
        {
            Settings settings = Settings.Default;
            if (settings != null)
            {
                ServerName = settings.ServerName;

                AuthenticationType = settings.IntegratedSecurity 
                    ? SqlServerAuthenticationType.Windows 
                    : SqlServerAuthenticationType.SqlServer;

                UserId = settings.UserId;
                DatabaseName = settings.DatabaseName;
                LibraryPath = settings.LibraryPath;
            }
        }

        private SqlServerAuthenticationType _authenticationType;

        public SqlServerAuthenticationType AuthenticationType
        {
            get => _authenticationType;
            set
            {
                if (_authenticationType == value)
                {
                    return;
                }

                _authenticationType = value;
                RaisePropertyChanged();

                LoadDatabaseNamesAsync();
            }
        }

        private string _databaseName;

        public string DatabaseName
        {
            get => _databaseName;
            set
            {
                if (string.Equals(_databaseName, value))
                {
                    return;
                }

                _databaseName = value;
                RaisePropertyChanged();
            }
        }

        private List<string> _databaseNames;

        public List<string> DatabaseNames
        {
            get => _databaseNames;
            private set
            {
                if (ReferenceEquals(_databaseNames, value))
                {
                    return;
                }

                _databaseNames = value;
                RaisePropertyChanged();
            }
        }

        private bool _databaseNamesIsLoading;

        public bool DatabaseNamesIsLoading
        {
            get => _databaseNamesIsLoading;
            private set
            {
                if (_databaseNamesIsLoading == value)
                {
                    return;
                }

                _databaseNamesIsLoading = value;
                RaisePropertyChanged();
            }
        }

        private string _libraryDirectoryPath;

        public string LibraryPath
        {
            get => _libraryDirectoryPath;
            set
            {
                if (string.Equals(_libraryDirectoryPath, value))
                {
                    return;
                }

                _libraryDirectoryPath = value;
                RaisePropertyChanged();
            }
        }

        private PasswordBox _passwordBox;

        public PasswordBox PasswordBox
        {
            get
            {
                if (_passwordBox is null)
                {
                    _passwordBox = new PasswordBox();
                    _passwordBox.LostFocus += PasswordBox_LostFocus;
                }
                return _passwordBox;
            }
        }

        private void PasswordBox_LostFocus(object sender, RoutedEventArgs e)
        {
            LoadDatabaseNamesAsync();
        }

        private string _serverName;

        public string ServerName
        {
            get => _serverName;
            set
            {
                if (string.Equals(_serverName, value))
                {
                    return;
                }

                _serverName = value;
                RaisePropertyChanged();

                LoadDatabaseNamesAsync();
            }
        }

        private List<string> _serverNames = new List<string>();

        public List<string> ServerNames
        {
            get => _serverNames;
            private set
            {
                if (ReferenceEquals(_serverNames, value))
                {
                    return;
                }

                _serverNames = value;
                RaisePropertyChanged();
            }
        }

        private bool _serverNamesIsLoading;

        public bool ServerNamesIsLoading
        {
            get => _serverNamesIsLoading;
            private set
            {
                if (_serverNamesIsLoading == value)
                {
                    return;
                }

                _serverNamesIsLoading = value;
                RaisePropertyChanged();
            }
        }

        private string _userId;

        public string UserId
        {
            get => _userId;
            set
            {
                if (string.Equals(_userId, value))
                {
                    return;
                }

                _userId = value;
                RaisePropertyChanged();

                LoadDatabaseNamesAsync();
            }
        }

        private CancellationTokenSource DatabaseNamesCts { get; set; }

        private CancellationTokenSource ServerNamesCts { get; set; }

        private ICommand _cancelCommand;

        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand<Window>(window => window?.Close()));

        private ICommand _clearPasswordCommand;

        public ICommand ClearPasswordCommand => _clearPasswordCommand ?? (_clearPasswordCommand = new RelayCommand(() => PasswordBox?.Clear()));

        private ICommand _clearUserIdCommand;

        public ICommand ClearUserIdCommand => _clearUserIdCommand ?? (_clearUserIdCommand = new RelayCommand(() => UserId = null));

        private ICommand _loadDatabaseNamesCommand;

        public ICommand LoadDatabaseNamesCommand => _loadDatabaseNamesCommand ?? (_loadDatabaseNamesCommand = new RelayCommand(LoadDatabaseNamesAsync));

        private ICommand _loadServerNamesCommand;

        public ICommand LoadServerNamesCommand => _loadServerNamesCommand ?? (_loadServerNamesCommand = new RelayCommand(LoadServerNamesAsync));

        private ICommand _okCommand;

        public ICommand OkCommand => _okCommand ?? (_okCommand = new RelayCommand<Window>(Ok));

        private ICommand _showFolderBrowserCommand;

        public ICommand ShowFolderBrowserCommand => _showFolderBrowserCommand ?? (_showFolderBrowserCommand = new RelayCommand<Window>(ShowFolderBrowser));

        private ICommand _windowClosingCommand;

        public ICommand WindowClosingCommand => _windowClosingCommand ?? (_windowClosingCommand = new RelayCommand(WindowClosing));

        private void CancelDatabaseNamesLoading()
        {
            if (DatabaseNamesCts != null)
            {
                try
                {
                    using (CancellationTokenSource cts = DatabaseNamesCts)
                    {
                        cts.Cancel();
                        DatabaseNamesCts = null;
                    }
                }
                catch (Exception)
                {
                    //
                }
            }
        }

        private void CancelServerNamesLoading()
        {
            if (ServerNamesCts != null)
            {
                try
                {
                    using (CancellationTokenSource cts = ServerNamesCts)
                    {
                        cts.Cancel();
                        ServerNamesCts = null;
                    }
                }
                catch (Exception)
                {
                    //
                }
            }
        }

        private static SqlConnection GetConnection(string serverName, SqlServerAuthenticationType authenticationType, string userId, SecureString password, string databaseName = null)
        {
            if (string.IsNullOrWhiteSpace(serverName))
            {
                return null;
            }

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder { DataSource = serverName };
            if (!string.IsNullOrWhiteSpace(databaseName))
            {
                builder.InitialCatalog = databaseName;
            }

            switch (authenticationType)
            {
                case SqlServerAuthenticationType.Windows:
                    builder.IntegratedSecurity = true;
                    break;

                case SqlServerAuthenticationType.SqlServer:
                    if (string.IsNullOrWhiteSpace(userId))
                    {
                        return null;
                    }

                    break;
            }

            SqlConnection connection = new SqlConnection(builder.ConnectionString);
            if (authenticationType == SqlServerAuthenticationType.SqlServer)
            {
                if (password is null)
                {
                    return null;
                }

                password.MakeReadOnly();
                connection.Credential = new SqlCredential(userId, password);
            }
            return connection;
        }

        private List<string> GetDatabaseNames(SecureString password)
        {
            List<string> databaseNames = new List<string>();

            SqlConnection connection = GetConnection(ServerName, AuthenticationType, UserId, password);
            if (connection is null)
            {
                return databaseNames;
            }

            DataTable table = null;
            try
            {
                using (connection)
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }
                    table = connection.GetSchema("Databases");
                }
            }
            catch (Exception)
            {
                //
            }
            finally
            {
                connection.Close();
            }

            if (table is null)
            {
                return databaseNames;
            }

            const string columnName = "database_name";
            if (table.Columns.Contains(columnName))
            {
                foreach (object item in table.Rows)
                {
                    if (item is DataRow row && row[columnName] is string databaseName && !string.IsNullOrWhiteSpace(databaseName))
                    {
                        databaseNames.Add(databaseName);
                    }
                }
            }

            databaseNames.Sort(AlphanumericComparer.Compare);
            return databaseNames;
        }

        private static List<string> GetServerNames()
        {
            List<string> names = new List<string>();

            const string serverNameColumn = "ServerName";
            const string instanceNameColumn = "InstanceName";

            DataTable table = SqlDataSourceEnumerator.Instance.GetDataSources();
            DataColumnCollection columns = table.Columns;

            if (columns.Contains(serverNameColumn) && columns.Contains(instanceNameColumn))
            {
                foreach (DataRow row in table.Rows)
                {
                    if (row[serverNameColumn] is string serverName && !string.IsNullOrWhiteSpace(serverName))
                    {
                        string name;
                        if (row[instanceNameColumn] is string instanceName && !string.IsNullOrWhiteSpace(instanceName))
                        {
                            name = $@"{serverName}\{instanceName}";
                        }
                        else
                        {
                            name = serverName;
                        }
                        names.Add(name);
                    }
                }
                names.Sort(AlphanumericComparer.Compare);
            }

            return names;
        }

        private async void LoadDatabaseNamesAsync()
        {
            DatabaseNames = new List<string>();
            CancelDatabaseNamesLoading();
            DatabaseNamesCts = new CancellationTokenSource();

            SecureString password = PasswordBox?.SecurePassword;

            try
            {
                await Task.Run(() =>
                {
                    DatabaseNamesIsLoading = true;
                    DatabaseNames = GetDatabaseNames(password);
                    DatabaseNamesIsLoading = false;
                },
                               DatabaseNamesCts.Token);
            }
            catch (Exception)
            {
                //
            }
        }

        private async void LoadServerNamesAsync()
        {
            ServerNames = new List<string>();
            CancelServerNamesLoading();
            ServerNamesCts = new CancellationTokenSource();

            try
            {
                await Task.Run(() =>
                {
                    ServerNamesIsLoading = true;
                    ServerNames = GetServerNames();
                    ServerNamesIsLoading = false;
                },
                               ServerNamesCts.Token);
            }
            catch (Exception)
            {
                //
            }
        }

        private void Ok(Window window)
        {
            if (string.IsNullOrWhiteSpace(LibraryPath) || string.IsNullOrWhiteSpace(DatabaseName))
            {
                return;
            }

            SqlConnection connection = GetConnection(ServerName, AuthenticationType, UserId, PasswordBox?.SecurePassword, DatabaseName);
            if (connection is null)
            {
                return;
            }

            SqlCommand command = new SqlCommand("SELECT 1", connection);

            try
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                command.ExecuteScalar();
            }
            catch (Exception)
            {
                return;
            }
            finally
            {
                connection.Close();
            }

            Singleton.Instance.Connection = connection;
            Singleton.Instance.LibraryPath = LibraryPath;

            Settings settings = Settings.Default;
            if (settings != null)
            {
                settings.ServerName = ServerName;

                switch (AuthenticationType)
                {
                    case SqlServerAuthenticationType.Windows:
                        settings.IntegratedSecurity = true;
                        settings.UserId = null;
                        break;

                    case SqlServerAuthenticationType.SqlServer:
                        settings.IntegratedSecurity = false;
                        settings.UserId = UserId;
                        break;

                    default:
                        break;
                }

                settings.DatabaseName = DatabaseName;
                settings.LibraryPath = LibraryPath;

                settings.Save();
            }

            window?.Close();
        }

        private void ShowFolderBrowser(Window window)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog
            {
                ShowNewFolderButton = true,
                Description = "Выберите папку библиотеки чертежей"
            };

            if (!string.IsNullOrWhiteSpace(LibraryPath) && Directory.Exists(LibraryPath))
            {
                dialog.SelectedPath = LibraryPath;
            }

            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                LibraryPath = dialog.SelectedPath;
            }
        }

        private void WindowClosing()
        {
            CancelServerNamesLoading();
            CancelDatabaseNamesLoading();
        }
    }
}
