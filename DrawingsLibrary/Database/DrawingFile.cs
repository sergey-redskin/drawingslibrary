﻿using DrawingsLibrary.Service;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace DrawingsLibrary.Database
{

    /// <summary>
    /// Файл чертежа.
    /// </summary>
    internal class DrawingFile : ViewModelBase
    {
        public DrawingFile(string hash, string extension, string name, string description)
        {
            Hash = hash;
            Extension = extension;
            Name = name;
            Description = description;
        }

        public DrawingFile(string hash, string extension, string sourceFilePath)
        {
            Hash = hash;
            Extension = extension;
            SourceFilePath = sourceFilePath;
        }

        private string _extension;

        /// <summary>
        /// Расширение имени файла.
        /// </summary>
        public string Extension
        {
            get => _extension;
            set
            {
                string lowerValue = value?.ToLower();
                if (string.Equals(_extension, lowerValue))
                {
                    return;
                }
                _extension = lowerValue;
                RaisePropertyChanged();
            }
        }

        private string _hash;

        /// <summary>
        /// Контрольная сумма файла, рассчитанная по алгоритму SHA-1.
        /// </summary>
        public string Hash
        {
            get => _hash;
            private set => _hash = value?.ToLower();
        }

        /// <summary>
        /// Путь к исходному файлу.
        /// </summary>
        private string SourceFilePath { get; }

        private string _description;

        /// <summary>
        /// Описание.
        /// </summary>
        public string Description
        {
            get => _description;
            set
            {
                if (string.Equals(_description, value))
                {
                    return;
                }
                _description = value;
                RaisePropertyChanged();
            }
        }

        private string _name;

        /// <summary>
        /// Имя.
        /// </summary>
        public string Name
        {
            get => _name;
            set
            {
                if (string.Equals(_name, value))
                {
                    return;
                }
                _name = value;
                RaisePropertyChanged();
            }
        }

        private ICommand _cancelCommand;

        /// <summary>
        /// Закрыть окно для редактирования параметров файла чертежа.
        /// </summary>
        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand<Window>(window => window?.Close()));

        private ICommand _saveCommand;

        /// <summary>
        /// Сохранить файл в библиотеке чертежей. Записать контрольную сумму в базу данных и скопировать файл в файловое хранилище.
        /// </summary>
        public ICommand SaveCommand => _saveCommand ?? (_saveCommand = new RelayCommand<Window>(Save));

        private void Save(Window window)
        {
            if (!Singleton.IsHash(Hash))
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(Extension))
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(Name))
            {
                return;
            }

            if (!string.IsNullOrWhiteSpace(SourceFilePath))
            {
                string filePath = Singleton.CompileFilePath(Hash, Extension);

                bool needCopy = false;
                if (File.Exists(filePath))
                {
                    string existingFileHash = Singleton.GetHash(filePath);
                    if (!string.Equals(existingFileHash, Hash, StringComparison.OrdinalIgnoreCase))
                    {
                        needCopy = true;
                    }
                }
                else
                {
                    needCopy = true;
                }

                if (needCopy)
                {
                    try
                    {
                        File.Copy(SourceFilePath, filePath, true);
                    }
                    catch (Exception)
                    {
                        return;
                    }
                }

                if (!File.Exists(filePath))
                {
                    return;
                }
            }

            DatabaseWriter.CallProcedure_AddOrUpdateDrawingFile(this);

            if (window != null)
            {
                window.DialogResult = true;
                window.Close();
            }
        }
    }
}
