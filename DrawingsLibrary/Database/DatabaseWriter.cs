﻿using DrawingsLibrary.Service;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DrawingsLibrary.Database
{
    internal static class DatabaseWriter
    {
        private static class AddOrUpdateDrawingFile
        {
            public static string hash;
            public static string extension;
            public static string name;
            public static string description;
        }

        private static class DeleteDrawingFile
        {
            public static string hash;
        }

        public static bool CallProcedure_AddOrUpdateDrawingFile(DrawingFile file)
        {
            if (file is null)
            {
                return false;
            }

            SqlCommand command = new SqlCommand($"dbo.{nameof(AddOrUpdateDrawingFile)}")
            {
                CommandType = CommandType.StoredProcedure
            };

            SqlParameterCollection parameters = command.Parameters;

            parameters.Add(nameof(AddOrUpdateDrawingFile.hash), SqlDbType.Char, 40).Value = file.Hash;
            parameters.Add(nameof(AddOrUpdateDrawingFile.extension), SqlDbType.NVarChar, 5).Value = file.Extension;
            parameters.Add(nameof(AddOrUpdateDrawingFile.name), SqlDbType.NVarChar, 256).Value = file.Name;
            parameters.Add(nameof(AddOrUpdateDrawingFile.description), SqlDbType.NVarChar, 1024).Value = file.Description;

            return CallProcedure(command);
        }

        public static bool CallProcedure_DeleteDrawingFile(DrawingFile file)
        {
            if (file is null)
            {
                return false;
            }

            SqlCommand command = new SqlCommand($"dbo.{nameof(DeleteDrawingFile)}")
            {
                CommandType = CommandType.StoredProcedure
            };

            SqlParameterCollection parameters = command.Parameters;

            parameters.Add(nameof(DeleteDrawingFile.hash), SqlDbType.Char, 40).Value = file.Hash;

            return CallProcedure(command);
        }

        private static bool CallProcedure(SqlCommand command)
        {
            SqlConnection connection = Singleton.Instance.Connection;
            if (connection is null)
            {
                return false;
            }

            const string resultParameterName = "result";

            command.Connection = connection;
            command.Parameters.Add(resultParameterName, SqlDbType.Bit).Direction = ParameterDirection.Output;

            object resultData;
            try
            {
                if (connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }
                command.ExecuteNonQuery();
                resultData = command.Parameters[resultParameterName].Value;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                connection.Close();
            }

            if (resultData is bool result)
            {
                return result;
            }

            return false;
        }
    }
}
