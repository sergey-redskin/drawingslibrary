﻿using DrawingsLibrary.Properties;
using DrawingsLibrary.Service;
using System.Windows;

namespace DrawingsLibrary
{
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Settings settings = Settings.Default;
            if (settings.UpgradeRequired)
            {
                settings.Upgrade();
                settings.UpgradeRequired = false;
                settings.Save();
            }
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            Singleton.Instance.Connection?.Dispose();
        }
    }
}
