﻿using GalaSoft.MvvmLight;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace DrawingsLibrary.Service
{
    public sealed class Singleton : ViewModelBase
    {
        private Singleton()
        {
        }

        private static Singleton _instance = null;
        private static readonly object padlock = new object();

        public static Singleton Instance
        {
            get
            {
                lock (padlock)
                {
                    if (_instance is null)
                    {
                        _instance = new Singleton();
                    }
                    return _instance;
                }
            }
        }

        private SqlConnection _connection;

        public SqlConnection Connection
        {
            get => _connection;
            internal set
            {
                if (ReferenceEquals(_connection, value))
                {
                    return;
                }

                if (_connection != null)
                {
                    _connection.Dispose();
                }

                _connection = value;
                RaisePropertyChanged();
            }
        }

        private string _libraryPath;

        public string LibraryPath
        {
            get => _libraryPath;
            internal set
            {
                if (string.Equals(_libraryPath, value))
                {
                    return;
                }

                _libraryPath = value;
                RaisePropertyChanged();
            }
        }

        public byte LevelCount
        {
            get;
        } = 2;

        public static string CompileFilePath(string hash, string extension)
        {
            if (!IsHash(hash))
            {
                return null;
            }

            if (string.IsNullOrWhiteSpace(extension))
            {
                return null;
            }

            string libraryPath = Instance.LibraryPath;
            if (string.IsNullOrWhiteSpace(libraryPath))
            {
                return null;
            }

            int levelCount = Instance.LevelCount;

            string[] parts = new string[levelCount + 1];
            parts[0] = libraryPath;
            for (int i = 0; i < levelCount; i++)
            {
                parts[i + 1] = hash[i].ToString();
            }

            try
            {
                string directoryPath = Path.Combine(parts);
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }
                return Path.Combine(directoryPath, $"{hash.Substring(levelCount)}{extension}");
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string GetHash(string filePath)
        {
            if (File.Exists(filePath))
            {
                try
                {
                    using (FileStream fileStream = new FileStream(filePath, FileMode.Open))
                    using (BufferedStream bufferedStream = new BufferedStream(fileStream))
                    using (SHA1Managed sha1 = new SHA1Managed())
                    {
                        byte[] hash = sha1.ComputeHash(bufferedStream);
                        StringBuilder stringBuilder = new StringBuilder(2 * hash.Length);
                        foreach (byte b in hash)
                        {
                            stringBuilder.AppendFormat("{0:x2}", b);
                        }
                        return stringBuilder.ToString();
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return null;
        }

        public static bool IsHash(string input)
        {
            if (string.IsNullOrWhiteSpace(input) || input.Length != 40)
            {
                return false;
            }

            Match match;
            try
            {
                match = Regex.Match(input, "^[0-9a-f]{40}$");
            }
            catch (Exception)
            {
                return false;
            }
            return match.Success;
        }
    }
}
