﻿using System.ComponentModel;

namespace DrawingsLibrary.Service
{
    internal enum SqlServerAuthenticationType
    {
        [Description("Проверка подлинности Windows")]
        Windows,

        [Description("Проверка подлинности SQL Server")]
        SqlServer
    }
}
