USE master;
GO

IF DB_ID('DrawingsLibrary') IS NOT NULL 
    DROP DATABASE DrawingsLibrary;

CREATE DATABASE DrawingsLibrary;
GO

USE DrawingsLibrary;
GO

CREATE TABLE dbo.DrawingFiles
(
    Hash char(40) NOT NULL,
    Extension nvarchar(5) NOT NULL,
    Name nvarchar(256) NOT NULL,
    Description nvarchar(1024),

    CONSTRAINT PK_Files PRIMARY KEY(Hash),
    CONSTRAINT CHK_Files_Hash CHECK(Hash LIKE REPLACE('0000000000000000000000000000000000000000', '0', '[0-9a-f]')),
    CONSTRAINT UNQ_Files_Name UNIQUE(Name)
);
GO

CREATE TABLE dbo.DrawingFilesChangeHistory
(
    Id int NOT NULL IDENTITY,
    Hash char(40) NOT NULL,
    DateTime datetime NOT NULL DEFAULT(CURRENT_TIMESTAMP),
    Username sysname NOT NULL DEFAULT(SUSER_SNAME()),
    Action nvarchar(14) NOT NULL,

    CONSTRAINT PK_DrawingFilesChangeHistory PRIMARY KEY(Id)
)

CREATE TABLE dbo.FileExtensions
(
    Extension nvarchar(5) NOT NULL,
    Description nvarchar(256),

    CONSTRAINT PK_FileExtensions PRIMARY KEY(Extension)
)

INSERT INTO dbo.FileExtensions (Extension, Description)
VALUES ('.dwg', '������ AutoCAD'),
       ('.stp', '���� STEP 3D')
GO

CREATE PROCEDURE dbo.AddOrUpdateDrawingFile
    @hash char(40) = NULL,
    @extension nvarchar(5) = NULL,
    @name nvarchar(256) = NULL,
    @description nvarchar(1024) = NULL,
    @result bit = 0 OUTPUT
AS
BEGIN
    IF TRIM(@hash) IS NULL
        RETURN;

    SET @hash = LOWER(@hash);
    IF NOT(@hash LIKE REPLACE('0000000000000000000000000000000000000000', '0', '[0-9a-f]'))
        RETURN;

    IF TRIM(@extension) IS NULL
        RETURN;

    IF LEN(@extension) > 5
        RETURN;

    SET @extension = LOWER(@extension);

    IF TRIM(@name) IS NULL
        RETURN;

    IF EXISTS(SELECT 1 FROM dbo.DrawingFiles WHERE Hash COLLATE DATABASE_DEFAULT = @hash)
    BEGIN
        DECLARE @existingExtension nvarchar(5);
        DECLARE @existingName nvarchar(256);
        DECLARE @existingDescription nvarchar(1024);

        SELECT @existingExtension = Extension, @existingName = Name, @existingDescription = Description
        FROM dbo.DrawingFiles
        WHERE Hash COLLATE DATABASE_DEFAULT = @hash;

        IF ISNULL(@extension, '') <> ISNULL(@existingExtension, '') OR ISNULL(@name, '') <> ISNULL(@existingName, '') OR ISNULL(@description, '') <> ISNULL(@existingDescription, '')
        BEGIN
            UPDATE dbo.DrawingFiles
            SET Extension = @extension, Name = @name, Description = @description
            WHERE Hash COLLATE DATABASE_DEFAULT = @hash;

            SET @result = 1;
        END
    END
    ELSE
    BEGIN
        INSERT INTO dbo.DrawingFiles(Hash, Extension, Name, Description) 
        VALUES (@hash, @extension, @name, @description);

        SET @result = 1;
    END
END
GO

CREATE PROCEDURE dbo.DeleteDrawingFile
    @hash char(40) = NULL,
    @result bit = 0 OUTPUT
AS
BEGIN
    IF TRIM(@hash) IS NULL
        RETURN;

    DELETE FROM dbo.DrawingFiles
    WHERE Hash COLLATE DATABASE_DEFAULT = @hash;

    SET @result = 1;
END
GO

CREATE TRIGGER DrawingFiles_InsertedAndDeleted
ON dbo.DrawingFiles
AFTER INSERT, DELETE
AS
BEGIN
    SET NOCOUNT ON;

    INSERT INTO dbo.DrawingFilesChangeHistory(Hash, Action)
    SELECT Hash, '��������' FROM inserted;

    INSERT INTO dbo.DrawingFilesChangeHistory(Hash, Action)
    SELECT Hash, '������' FROM deleted;
END
GO

CREATE TRIGGER DrawingFiles_Updated
ON dbo.DrawingFiles
AFTER Update
AS
BEGIN
    SET NOCOUNT ON;

    INSERT INTO dbo.DrawingFilesChangeHistory(Hash, Action)
    SELECT Hash, '��������������' FROM inserted;
END
GO